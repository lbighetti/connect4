package com.spartancoder.connect4.controller;

import com.spartancoder.connect4.App;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(App.class)
@WebAppConfiguration
public class GameRestControllerTest {

    protected MockMvc mockMvc;
    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetGameSuccessfully() throws Exception {
        mockMvc.perform(get("/newgame")
                        .param("username","john"));

        mockMvc.perform(get("/getgame")
                .param("username","john")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetGameWrongUser() throws Exception {
        mockMvc.perform(get("/getgame")
                .param("username","jarvis")).andExpect(status().isBadRequest());
    }

    @Test
    public void testNewGame() throws Exception {
        mockMvc.perform(get("/newgame")
                .param("username","john")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testPlayValid() throws Exception {
        mockMvc.perform(get("/newgame")
                .param("username","john"));

        mockMvc.perform(get("/play")
                .param("username","john").param("column","1")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testPlayBadPlayer() throws Exception {
        mockMvc.perform(get("/play")
                .param("username","jarvis").param("column","1")).andExpect(status().isBadRequest());
    }
}