package com.spartancoder.connect4.service;

import com.spartancoder.connect4.App;
import com.spartancoder.connect4.domain.Game;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = App.class)
public class GameServiceTest {

    @Autowired
    private GameService gameService;

    @Test
    public void testPlay() throws Exception {
        this.gameService.createGame("john");
        this.gameService.play("john", 1);
        Game retrievedGame = this.gameService.retrieveGame("john");
        Game.BoardStatus[][] retrievedBoard = retrievedGame.getBoard();
        assertEquals(Game.BoardStatus.PLAYER, retrievedBoard[0][0]);
    }

    @Test
    public void testRetrieveGame() throws Exception {
        this.gameService.createGame("john");
        Game retrievedGame = this.gameService.retrieveGame("john");
        assertEquals("john", retrievedGame.getUsername());
    }

    @Test
    public void testCreateGame() throws Exception {
        Game createdGame = this.gameService.createGame("john");
        assertEquals("john", createdGame.getUsername());
    }
}