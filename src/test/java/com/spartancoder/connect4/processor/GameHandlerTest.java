package com.spartancoder.connect4.processor;

import com.spartancoder.connect4.App;
import com.spartancoder.connect4.domain.Game;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = App.class)
public class GameHandlerTest {

    public static final int VALID_COLUMN_NUMBER = 7;
    public static final int VALID_COLUMN_INDEX = 6;
    public static final int FIRST_ROW_INDEX = 0;
    public static final int INVALID_COLUMN = 8;
    public static final String USERNAME = "john";

    @Autowired
    private GameHandler gameHandler;
    private Game game;

    @Before
    public void setUp() throws Exception {
        this.game = new Game();
        this.gameHandler.initializeOrResetGame(this.game, USERNAME);
    }

    @Test
    public void testPlayInvalidColumnOutOfRange() throws Exception {
        boolean success = this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, INVALID_COLUMN);
        assertFalse(success);
    }

    @Test
    public void testPlayInvalidGameStatus() throws Exception {
        this.game.setGameStatus(Game.GameStatus.DRAW);
        boolean success = this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, VALID_COLUMN_NUMBER);
        assertFalse(success);
    }

    @Test
    public void testPlayValidColumn() throws Exception {
        boolean success = this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, VALID_COLUMN_NUMBER);
        Game.BoardStatus[][] board = this.game.getBoard();
        assertTrue(success);
        assertEquals(Game.BoardStatus.PLAYER, board[FIRST_ROW_INDEX][VALID_COLUMN_INDEX]);
    }

    @Test
    public void testPlayInvalidFullColumn() throws Exception {
        Game.BoardStatus[][] board = new Game.BoardStatus[Game.NUMBER_OF_ROWS][Game.NUMBER_OF_COLUMNS];
        for (int i = 0; i < Game.NUMBER_OF_ROWS; i++) {
            board[i][VALID_COLUMN_INDEX] = Game.BoardStatus.PLAYER;
        }
        this.game.setBoard(board);
        boolean success = this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, VALID_COLUMN_NUMBER);
        assertFalse(success);
    }

    @Test
    public void testResetBoardStatus() throws Exception {
        for (Game.BoardStatus[] row : this.game.getBoard()) {
            for (Game.BoardStatus cell : row) {
                assertEquals(Game.BoardStatus.NONE, cell);
            }
        }
    }

    @Test
    public void testCheckForWinnerHorizontalPlayerWin() throws Exception {
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        assertEquals(Game.GameStatus.PLAYER_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerHorizontalOpponentWin() throws Exception {
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 4);
        assertEquals(Game.GameStatus.OPPONENT_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerNoWinners() throws Exception {
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 4);
        assertEquals(Game.GameStatus.ONGOING, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerVerticalOpponentWins() throws Exception {
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 1);
        assertEquals(Game.GameStatus.OPPONENT_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerVerticalPlayerWins() throws Exception {
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        assertEquals(Game.GameStatus.PLAYER_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerDiagonalPlayerWinsLeftToRightUpwards() {
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 1);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 3);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 5);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        assertEquals(Game.GameStatus.PLAYER_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerDiagonalPlayerWinsRightToLeftUpwards() {
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 7);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 6);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 6);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 5);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 5);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 5);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        this.gameHandler.play(this.game, Game.BoardStatus.OPPONENT, 2);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, 4);
        assertEquals(Game.GameStatus.PLAYER_WINS, this.game.getGameStatus());
    }

    @Test
    public void testCheckForWinnerDraw() {

        Game.BoardStatus[][] board = new Game.BoardStatus[Game.NUMBER_OF_ROWS][Game.NUMBER_OF_COLUMNS];
        Game.BoardStatus lastPlayer = Game.BoardStatus.OPPONENT;

        for (int row = 0; row < Game.NUMBER_OF_ROWS; row++) {
            for (int column = 0; column < Game.NUMBER_OF_COLUMNS; column++) {
                if (lastPlayer.equals(Game.BoardStatus.PLAYER)) {
                    board[row][column] = Game.BoardStatus.OPPONENT;
                    lastPlayer = Game.BoardStatus.OPPONENT;
                } else {
                    board[row][column] = Game.BoardStatus.PLAYER;
                    lastPlayer = Game.BoardStatus.PLAYER;
                }
            }
        }
        board[Game.NUMBER_OF_ROWS - 1][Game.NUMBER_OF_COLUMNS - 1] = Game.BoardStatus.NONE; //only this spot left to play
        this.game.setBoard(board);
        this.gameHandler.play(this.game, Game.BoardStatus.PLAYER, Game.NUMBER_OF_COLUMNS); //play last remaining spot
        assertEquals(Game.GameStatus.DRAW, this.game.getGameStatus());
    }
}