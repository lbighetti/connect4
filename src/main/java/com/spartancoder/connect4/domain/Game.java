package com.spartancoder.connect4.domain;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@ApiObject
@Document
public class Game {
    public static final int NUMBER_OF_ROWS = 6;
    public static final int NUMBER_OF_COLUMNS = 7;

    @ApiObjectField(description = "Status that defines the winner")
    private GameStatus gameStatus;

    @ApiObjectField(description = "Status of the board")
    private BoardStatus[][] board;


    @ApiObjectField(description = "User who owns this particular game")
    @Id
    private String username;


    public enum BoardStatus {
        PLAYER, OPPONENT, NONE
    }

    public enum GameStatus {
        PLAYER_WINS, OPPONENT_WINS, DRAW, ONGOING
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public BoardStatus[][] getBoard() {
        return board;
    }
    public void setBoard(BoardStatus[][] board) {
        this.board = board;
    }

}
