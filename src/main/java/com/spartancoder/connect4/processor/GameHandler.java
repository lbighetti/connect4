package com.spartancoder.connect4.processor;

import com.spartancoder.connect4.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GameHandler {

    Logger logger = LoggerFactory.getLogger(GameHandler.class);

    public boolean play(Game game, Game.BoardStatus player, int targetColumn) {
        if (! isColumnValid(game, targetColumn)){
            logger.info("Target Column is not valid, user" + game.getUsername() + ", column: " + targetColumn);
            return false;
        }

        int targetColumnIndex = targetColumn - 1;
        int maxRowIndex = Game.NUMBER_OF_ROWS - 1;
        boolean success = false;
        Integer targetRow = null;

        for (int row = maxRowIndex; row >= 0; row--) {
            if (game.getBoard()[row][targetColumnIndex] == Game.BoardStatus.NONE) {
                targetRow = row;
                success = true;
            }
        }

        if (success) {
            Game.BoardStatus[][] newBoard = game.getBoard();
            newBoard[targetRow][targetColumnIndex] = player;
            game.setBoard(newBoard);
            this.updateWinner(game);
            return true;
        } else {
            return false;
        }
    }

    public boolean isColumnValid(Game game, int targetColumn) {
        boolean validColumnRange = targetColumn > 0 && targetColumn <= Game.NUMBER_OF_COLUMNS;
        if (!validColumnRange) {
            return false;
        }
        if (!game.getGameStatus().equals(Game.GameStatus.ONGOING)) {
            return false;
        }
        if(game.getBoard()[Game.NUMBER_OF_ROWS - 1][targetColumn - 1] != Game.BoardStatus.NONE){
            return false;
        }
        return true;
    }

    public void resetBoardStatus(Game game) {
        Game.BoardStatus[][] board = new Game.BoardStatus[Game.NUMBER_OF_ROWS][Game.NUMBER_OF_COLUMNS];
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                board[row][column] = Game.BoardStatus.NONE;
            }
        }
        game.setBoard(board);
    }

    public void initializeOrResetGame(Game game, String username) {
        game.setGameStatus(Game.GameStatus.ONGOING);
        resetBoardStatus(game);
        game.setUsername(username);
    }

    public void updateWinner(Game game) {
        Game.BoardStatus winner;
        Game.BoardStatus[][] board = game.getBoard();

        winner = checkForWinnerHorizontal(board);
        if (winner != Game.BoardStatus.NONE) {
            updateGameWithCorrectWinner(game, winner);
        }

        winner = checkForWinnerVertical(board);
        if (winner != Game.BoardStatus.NONE) {
            updateGameWithCorrectWinner(game, winner);
        }

        winner = checkForWinnerDiagonals(board);
        if (winner != Game.BoardStatus.NONE) {
            updateGameWithCorrectWinner(game, winner);
        }

        if (checkForDraw(board)) {
            game.setGameStatus(Game.GameStatus.DRAW);
        }
    }

    private boolean checkForDraw(Game.BoardStatus[][] board) {
        for (Game.BoardStatus[] row : board) {
            for (Game.BoardStatus cell : row) {
                if (cell == Game.BoardStatus.NONE) {
                    return false;
                }
            }
        }
        return true;
    }


    private void updateGameWithCorrectWinner(Game game, Game.BoardStatus winner) {
        if (winner == Game.BoardStatus.PLAYER) {
            game.setGameStatus(Game.GameStatus.PLAYER_WINS);
        }
        if (winner == Game.BoardStatus.OPPONENT) {
            game.setGameStatus(Game.GameStatus.OPPONENT_WINS);
        }
    }

    private Game.BoardStatus checkForWinnerHorizontal(Game.BoardStatus[][] board) {

        Game.BoardStatus winner;

        for (Game.BoardStatus[] row : board) {
            for (int column = 0; column < row.length - 3; column++) {

                if (row[column] == row[column + 1]
                        && row[column] == row[column + 2]
                        && row[column] == row[column + 3]) {

                    winner = row[column];

                    if (winner != Game.BoardStatus.NONE) {
                        return row[column];
                    }
                }
            }
        }
        return Game.BoardStatus.NONE;
    }

    private Game.BoardStatus checkForWinnerVertical(Game.BoardStatus[][] board) {

        Game.BoardStatus winner;

        for (int row = 0; row < Game.NUMBER_OF_ROWS - 3; row++) {
            for (int column = 0; column < Game.NUMBER_OF_COLUMNS; column++) {

                if (board[row][column] == board[row + 1][column]
                        && board[row][column] == board[row + 2][column]
                        && board[row][column] == board[row + 3][column]) {

                    winner = board[row][column];

                    if (winner != Game.BoardStatus.NONE) {
                        return board[row][column];
                    }
                }
            }
        }
        return Game.BoardStatus.NONE;
    }

    private Game.BoardStatus checkForWinnerDiagonals(Game.BoardStatus[][] board) {
        Game.BoardStatus winner;

        //Left to right, upwards
        for (int row = 0; row < Game.NUMBER_OF_ROWS - 3; row++) {
            for (int column = 0; column < Game.NUMBER_OF_COLUMNS - 3; column++) {

                if (board[row][column] == board[row + 1][column + 1]
                        && board[row][column] == board[row + 2][column + 2]
                        && board[row][column] == board[row + 3][column + 3]) {

                    winner = board[row][column];

                    if (winner != Game.BoardStatus.NONE) {
                        return board[row][column];
                    }
                }
            }
        }

        //Right to Left, upwards
        for (int row = 0; row < Game.NUMBER_OF_ROWS - 3; row++) {
            for (int column = 3; column < Game.NUMBER_OF_COLUMNS; column++) {

                if (board[row][column] == board[row + 1][column - 1]
                        && board[row][column] == board[row + 2][column - 2]
                        && board[row][column] == board[row + 3][column - 3]) {

                    winner = board[row][column];

                    if (winner != Game.BoardStatus.NONE) {
                        return board[row][column];
                    }
                }
            }
        }

        return Game.BoardStatus.NONE;
    }
}
