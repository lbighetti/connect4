package com.spartancoder.connect4.processor;

import com.spartancoder.connect4.domain.Game;

public class OpponentAI extends GameHandler {

    static final int MAX_DEPTH = 8;
    static final float WIN_REVENUE = 1f;
    static final float LOSE_REVENUE = -1f;
    static final float UNCERTAIN_REVENUE = 0f;

    private Game game;
    public int getOpponentNextMove() {
        double maxValue = 2. * Integer.MIN_VALUE;
        int move = 0;
        for (int column = 0; column < Game.NUMBER_OF_COLUMNS; column++) {
            if (this.isColumnValid(game, column)) {
                double value = moveValue(column);
                if (value > maxValue) {
                    maxValue = value;
                    move = column;
                    if (value == WIN_REVENUE) {
                        break;
                    }
                }
            }
        }
        return move;
    }

    double moveValue(int column) {
        this.play(game, Game.BoardStatus.OPPONENT, column);
        double val = alphabeta(MAX_DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE, false);
        this.unPlay(column);
        return val;
    }

    double alphabeta(int depth, double alpha, double beta, boolean maximizingPlayer) {
        Game.GameStatus gameStatus = game.getGameStatus();
        boolean hasWinner = (gameStatus != Game.GameStatus.ONGOING) && (gameStatus != Game.GameStatus.DRAW);
        if (depth == 0 || hasWinner) {
            double score;
            if (hasWinner) {
                score = gameStatus == Game.GameStatus.PLAYER_WINS ? LOSE_REVENUE : WIN_REVENUE;
            } else {
                score = UNCERTAIN_REVENUE;
            }
            return score / (MAX_DEPTH - depth + 1);
        }

        if (maximizingPlayer) {
            for (int column = 0; column < Game.NUMBER_OF_COLUMNS; column++) {
                if (this.isColumnValid(game, column)) {
                    this.play(game, Game.BoardStatus.OPPONENT, column);
                    alpha = Math.max(alpha, alphabeta(depth - 1, alpha, beta, false));
                    this.unPlay(column);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return alpha;
        } else {
            for (int column = 0; column < Game.NUMBER_OF_COLUMNS; column++) {
                if (this.isColumnValid(game, column)) {
                    this.play(game, Game.BoardStatus.PLAYER, column);
                    beta = Math.min(beta, alphabeta(depth - 1, alpha, beta, true));
                    this.unPlay(column);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return beta;
        }
    }

    public void unPlay(int targetColumn) {
        int columnIndex = targetColumn - 1;
        Game.BoardStatus[][] board = game.getBoard();
        for (int row = Game.NUMBER_OF_ROWS - 1; row >= 0; row--) {
            if (game.getBoard()[row][columnIndex] != Game.BoardStatus.NONE) {
                board[row][columnIndex] = Game.BoardStatus.NONE;
            }
        }
        game.setBoard(board);
        this.updateWinner(game);
    }

    public OpponentAI(Game game) {
        this.game = game;
    }
}
