package com.spartancoder.connect4.controller;

import com.spartancoder.connect4.domain.Game;
import com.spartancoder.connect4.service.GameService;
import org.jsondoc.core.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(name = "connect4 services", description = "Methods for controlling Connect4 game actions")
@RestController
public class GameRestController {

    @Autowired
    private GameService gameService;

    Logger logger = LoggerFactory.getLogger(GameRestController.class);

    @ApiErrors(apierrors = @ApiError(code = "400", description = "User does not have an ongoing game"))
    @ApiMethod(description = "Retrieves ongoing game for the user.")
    @RequestMapping("/getgame")
    @ResponseBody
    public @ApiResponseObject ResponseEntity<Game> getGame(@ApiQueryParam(name = "username", description = "the username") @RequestParam String username){

        ResponseEntity<Game> response;
        Game retrievedGame = gameService.retrieveGame(username);

        if (retrievedGame == null){
            logger.warn("User "  + username + " does not have an ongoing game");

            response = new ResponseEntity<>(retrievedGame, HttpStatus.BAD_REQUEST);
        } else {
            logger.debug("User " + username + "'s game was found, returning.");

            response = new ResponseEntity<>(retrievedGame, HttpStatus.OK);
        }
        return response;
    }

    @ApiMethod(description = "Creates a new game and returns it")
    @RequestMapping("/newgame")
    @ResponseBody
    public @ApiResponseObject ResponseEntity<Game> newGame(@ApiQueryParam(name = "username", description = "the username") @RequestParam String username){
        ResponseEntity<Game> response = new ResponseEntity<>(gameService.createGame(username), HttpStatus.OK);

        logger.debug("New game created for user " + username);

        return response;
    }


    @ApiErrors(apierrors = @ApiError(code = "400", description = "User does not have an ongoing game"))
    @ApiMethod(description = "Attempts a play for the current game")
    @RequestMapping("/play")
    @ResponseBody
    public @ApiResponseObject  ResponseEntity<Game> play(@ApiQueryParam(name = "username", description = "the username") @RequestParam String username, @ApiQueryParam(name = "column", description = "the column intended to be played") @RequestParam int column){
        Game retrievedGame = this.gameService.retrieveGame(username);
        ResponseEntity<Game> response;
        if (retrievedGame == null){

            logger.warn("User "  + username + " does not have an ongoing game. Play was not performed");

            response = new ResponseEntity<>(retrievedGame, HttpStatus.BAD_REQUEST);
        } else {
            logger.debug("User " + username + "'s game was found, attempting play in column: " + column);

            Game resultGame = this.gameService.play(username, column);
            response = new ResponseEntity<>(resultGame, HttpStatus.OK);
        }
        return response;
    }
}
