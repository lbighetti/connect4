package com.spartancoder.connect4.service;

import com.spartancoder.connect4.processor.GameHandler;
import com.spartancoder.connect4.domain.GameRepository;
import com.spartancoder.connect4.processor.OpponentAI;
import com.spartancoder.connect4.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameHandler gameHandler;

    Logger logger = LoggerFactory.getLogger(GameService.class);

    public Game play(String username, Integer column){
        Game game = this.retrieveGame(username);
        boolean playerMoveSuccessful = gameHandler.play(game, Game.BoardStatus.PLAYER, column);
        gameRepository.save(game);

        if(playerMoveSuccessful){

            logger.debug("Player " + username + " successful on column " + column);

            OpponentAI opponentAI = new OpponentAI(game);
            int opponentMove = opponentAI.getOpponentNextMove();
            game = this.retrieveGame(username);
            gameHandler.play(game, Game.BoardStatus.OPPONENT, opponentMove);

            logger.debug("Player " + username + " opponent has played on column " + column);
        }

        gameRepository.save(game);
        return game;
    }

    public Game retrieveGame(String username){
        return gameRepository.findByUsername(username);
    }

    public Game createGame(String username){
        Game game = new Game();
        gameHandler.initializeOrResetGame(game, username);
        this.gameRepository.save(game);

        logger.debug("Created new game for player " + username);

        return game;
    }
}
